const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_netflix"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament( nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60), abonament VARCHAR(20), nrCard VARCHAR(40),cvv INTEGER, varsta VARCHAR(3), cnp VARCHAR(20), sex CHAR(1))";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let abonament = req.body.abonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let sex = 'M';
    let error = [];
    
    if(!nume || !prenume || !telefon || !email || !facebook || !abonament || !nrCard || !cvv || !varsta || !cnp)
   {
        error.push("Nu s-au completat toate campurile obligatorii! ");
        console.log("Nu s-au completat toate campurile obligatorii! ");
   }
   else
    {
        if (nume.lenght <= 2 || nume.lenght > 20)
        {
            error.push("Nume invalid!");
            console.log("Nume invalid!");
        } 
        else if(!nume.match("^[A-Za-z]+$"))
        {
            error.push("Numele trebuie sa contina doar litere!");
            console.log("Numele trebuie sa contina doar litere!");
        }

        if (prenume.length <=  2 || prenume.lenght > 20)
        {
            error.push("Prenume invalid!");
            console.log("Prenume invalid!");
        }
        else if(!prenume.match("^[A-Za-z]+$"))
        {
            error.push("Prenumele trebuie sa contina doar litere!");
            console.log("Prenumele trebuie sa contina doar litere!");
        }

        if (telefon.lenght < 10)
        {
            error.push("Numarul de telefon trebuie sa aiba minim 10 cifre!");
            console.log("Numarul de telefon trebuie sa aiba minim 10 cifre!");
        }
        else if (!telefon.match("^[0-9]+$"))
        {
            error.push("Numarul de telefon trebuie sa contina doar cifre!");
            console.log("Numarul de telefon trebuie sa contina doar cifre!");
        }

        if(!email.includes("@gmail.com") && !email.includes("@yahoo.com"))
        {
            error.push("Email necorespunzator!");
            console.log("Email necorespunzator!");
        }

        if(!facebook.includes("facebook.com"))
        {
            error.push("Link de facebook invalid!");
            console.log("Link de facebook invalid!");
        }

        if(!abonament.match("^[0-9]+$"))
        {
            error.push("Tip abonament gresit!");
            console.log("Tip abonament gresit!");
        }

        if (nrCard.lenght < 16 || nrCard.lenght > 16)
        {
            error.push("Numarul cardului trebuie sa contina 16 cifre!");
            console.log("Numarul cardului trebuie sa contina 16 cifre!");
        }
        else if(!nrCard.match("^[0-9]+$"))
        {
            error.push("Numarul cardului trebuie sa contina doar cifre!");
            console.log("Numarul cardului trebuie sa contina doar cifre!");
        }

        if (cvv.lenght < 3 || cvv.lenght > 3)
        {
            error.push("Cvv invalid!");
            console.log("Cvv invalid!");
        }
        else if (parseInt(cvv) == "NaN")
        {
            error.push("Cvv trebuie sa contina doar cifre!");
            console.log("Cvv trebuie sa contina doar cifre");
        }

        if (varsta.lenght < 0 || varsta.lenght > 999)
        {
            error.push("Varsta este incorecta!");
            console.log("Varsta este incorecta!");
        }
        else if(!varsta.match("^[0-9]+$"))
        {
            error.push("Varsta trebuie sa contina doar cifre!");
            console.log("Varsta trebuie sa contina doar cifre!");
        }

        if (cnp.lenght < 13 || cnp.lenght > 13 )
        {
            error.push("Cnp-ul trebuie sa contina 13 cifre!");
            console.log("Cnp-ul trebuie sa contina 13 cifre!");
        }
        else if (!cnp.match("^[0-9]+$"))
        {
            error.push("Cnp-ul trebuie sa contina doar cifre!");
            console.log("Cnp-ul trebuie sa contina doar cifre!");
        }

        if(cnp[0] == 2 || cnp[0] == 4 || cnp[0] == 6)
            sex = 'F';

        var today = new Date();
        var an = today.getFullYear();
        var luna = today.getMonth();
        var ziua = today.getDate();
        
        var an_cnp = 0, luna_cnp = 0, ziua_cnp = 0;
        var p = 1;
        
        an_cnp = an_cnp + p * cnp[2] + (p * 10) * cnp[1];
        luna_cnp = luna_cnp + p * cnp[4] + (p * 10) * cnp[3];
        ziua_cnp = ziua_cnp + p * cnp[6] + (p * 10) * cnp[5];

        var ok = 1;
        var varsta_int = Number(varsta);
    
        if(varsta_int != (an - an_cnp) % 100)
            ok = 0;
        
        if(ok == 1) //anul e ok
            if(varsta_int == (an - an_cnp) % 100)   //an e ok 
                if(luna + 1 < luna_cnp) //comparam lunile ca sa fie ok
                    ok = 0;
        if(ok == 1)
            if(varsta_int == (an - an_cnp) % 100)   //an ok 
                if(luna + 1 == luna_cnp) // luna ok
                    if(ziua < ziua_cnp) // comparam zilele sa fie ok
                        ok = 0;
        if( ok == 0)
        {
            error.push("Varsta nu conicide cu cnp-ul!");
            console.log("Varsta nu coincide cu cnp-ul!");
        }
        
    }


    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,abonament,nrCard,cvv,varsta, CNP, sex) VALUES('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + abonament +"','" + nrCard +"','" +cvv+"','"+ varsta + "','" + cnp + "','" + sex +"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error);
        console.log("Eroare la inserarea in baza de date!");
    }
});